import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    RouterStateSnapshot,
    UrlTree,
    CanActivate,
    Router,
} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { LoadingService } from '../core/services/loadingSpinner/loading.service';

@Injectable({
    providedIn: 'root',
})
export class NavGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private router: Router,
        private loading: LoadingService,
    ) {}

    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ):
        | Observable<boolean | UrlTree>
        | Promise<boolean | UrlTree>
        | boolean
        | UrlTree {
        this.loading.show();
        this.loading.hide();
        if (this.authService.isLogged()) {
            this.authService.redirectUrl = null;

            return true;
        }

        this.authService.redirectUrl = state.url;
        this.router.navigate(['']);
        return false;
    }
}
