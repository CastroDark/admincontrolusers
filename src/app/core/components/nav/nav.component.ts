import { Component, OnInit, Input } from '@angular/core';
import {
    NavigationService,
    Page,
} from '../../services/navigation/navigation.service';
import { NavRoute } from '../../../nav-routing';
import { AuthService } from '../../../auth/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HostListener } from '@angular/core';
import { StorageService } from '../../services/storage/storage.service';
import { LoadingService } from '../../services/loadingSpinner/loading.service';
import { AlertManagerService } from '../../services/alertSweet/alertManager.service';

@Component({
    selector: 'app-nav',
    templateUrl: './nav.component.html',
    styleUrls: ['./nav.component.scss'],
})
export class NavComponent implements OnInit {
    // @Input() href;
    // preventDefault(event) {
    //     if (this.href.length == 0) event.preventDefault();
    // }
    isOpen = true;
    nameCompany: string = '';
    screenHeight: number;
    screenWidth: number;

    opened = true;
    over = 'side';
    expandHeight = '42px';
    collapseHeight = '42px';
    displayMode = 'flat';

    constructor(
        private navigationService: NavigationService,
        private authService: AuthService,
        private router: Router,
        private storage: StorageService,
        private loading: LoadingService,
        private alert: AlertManagerService,
    ) {}

    ngOnInit() {
        this.getScreenSize();

        setTimeout(() => {
            this.nameCompany = this.storage.getProfileCompany().shortName;
        }, 1000);
    }

    @HostListener('window:resize', ['$event'])
    getScreenSize(event?) {
        this.screenHeight = window.innerHeight;
        this.screenWidth = window.innerWidth;
        //console.log(this.screenHeight, this.screenWidth);

        if (this.screenWidth <= 1000) {
            this.toggleSideNav();
            this.isOpen = false;
        } else {
            this.isOpen = true;
        }
    }

    public toggleSideNav() {
        this.isOpen = !this.isOpen;
    }

    public getNavigationItems(): NavRoute[] {
        return this.navigationService.getNavigationItems();
    }

    public getActivePage(): Page {
        return this.navigationService.getActivePage();
    }

    public logout() {
        this.alert.showALertTimer('Saliendo Del Sistema..').then(exec => {
            this.loading.show();

            this.authService.logout();
            setTimeout(() => {
                this.loading.hide();
                this.router.navigate(['login'], { replaceUrl: true });
            }, 1000);
        });
    }

    public getPreviousUrl(): string[] {
        return this.navigationService.getPreviousUrl();
    }

    public navigate(url: string) {
        console.log('sd');
        this.router.navigate([url]);
    }
}
