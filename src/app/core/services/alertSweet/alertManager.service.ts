import { Injectable, EventEmitter } from '@angular/core';
// ES6 Modules or TypeScript
import Swal from 'sweetalert2';
import {
    IUserOnline,
    IProfileCompany,
} from '../../../Interfaces/interfacesPublics';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
    providedIn: 'root',
})
export class AlertManagerService {
    constructor(private modalService: NgbModal) {}

    showQuestion({
        title,
        text,
        msgFinish,
    }: {
        title?: string;
        text?: string;
        msgFinish: boolean;
    }): Promise<any> {
        const promise = new Promise<any>((resolve, reject) => {
            if (title === undefined || title === null) {
                title = 'Estas Seguro..';
            }
            if (text === undefined || text === null) {
                text = 'Ejecutar Accion..';
            }

            Swal.fire({
                title: `${title}?`,
                text: `${text}!`,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Estas Seguro. !',
                cancelButtonText: 'No',
            }).then(result => {
                if (result.value) {
                    resolve('1');

                    if (msgFinish) {
                        this.showMsgSuccess();
                    }
                    // this.emitConfirmSelect(1);
                    // Swal.fire(
                    //     'Deleted!',
                    //     'Your imaginary file has been deleted.',
                    //     'success',
                    // );
                    // For more information about handling dismissals please visit
                    // https://sweetalert2.github.io/#handling-dismissals
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    if (msgFinish) {
                        Swal.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error',
                        );
                    }

                    reject('reject');
                }
            });
        });

        return promise;
        // Swal.fire('Hello world!');
    }

    showMsgSuccess() {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Operacion Exitosa...',
            showConfirmButton: false,
            timer: 1500,
        });
    }

    showALertTimer(title?: string, text?: string): Promise<any> {
        const promise = new Promise<any>((resolve, reject) => {
            if (title === undefined || title === null) {
                title = 'Ejecutando...';
            }
            let timerInterval;
            Swal.fire({
                title: title,
                html: ' Espere!! &nbsp <b></b> milisegundos.',
                timer: 2000,
                timerProgressBar: true,
                onBeforeOpen: () => {
                    Swal.showLoading();
                    timerInterval = setInterval(() => {
                        Swal.getContent().querySelector(
                            'b',
                        ).textContent = String(Swal.getTimerLeft());
                    }, 100);
                },
                onClose: () => {
                    // reject(timerInterval);
                    clearInterval(timerInterval);
                },
            }).then(result => {
                if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.timer
                ) {
                    resolve('execute');
                    //console.log('I was closed by the timer'); // eslint-disable-line
                }
            });
        });

        return promise;
        // Swal.fire('Hello world!');
    }

    showMsgFailed(title: string) {
        Swal.fire({
            icon: 'error',
            title: 'Ocurrio un Error...',
            text: title,
            // footer: '<a href>Why do I have this issue?</a>',
        });
    }
}
