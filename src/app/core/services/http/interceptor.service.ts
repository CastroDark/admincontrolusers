import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http';
import { AuthService } from '../../../auth/auth.service';
import { Observable } from 'rxjs';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(
        private authService: AuthService,
        private storage: StorageService,
    ) {}

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler,
    ): Observable<HttpEvent<any>> {
        const token = this.storage.getToken(); //  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJVc2VyRGF0YSI6IntcIklkXCI6NzY2Nzc4ODk5MjMzMi4wLFwiSWRSb2xcIjoxMTExMS4wLFwiVW5pcXVlSWRcIjpcImExNWY4ZTlmLTRiMWUtNGFkZi1hNGE2LWFlN2FkY2JiNTQ2M1wiLFwiSWRDb21wYW55XCI6XCI0NGVhZmI5NjZjZmI0ODVhYWY0NC1lMjhmOWZiYjY0MGNcIixcIlJlbWVuYmVyXCI6ZmFsc2V9IiwibmJmIjoxNTc4OTY5ODU1LCJleHAiOjE1Nzg5Njk5NzUsImlzcyI6Imh0dHBzOi8vMTAuMTAuMC4xMDE6NDQzNTIiLCJhdWQiOiJBdXRoV2ViQXBpRGVtbyJ9.KcOZSDNVwx3thHvRGXvtN3AmS-Tju9z_2rXXhVeZYoo';

        if (token) {
            req = req.clone({
                setHeaders: { Authorization: `Bearer ${token}` },
            });
            // console.log(req);
        }

        return next.handle(req);
    }
}
