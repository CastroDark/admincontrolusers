import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-homepage3',
    templateUrl: './homepage3.component.html',
    styleUrls: ['./homepage3.component.scss'],
})
export class Homepage3Component implements OnInit {
    constructor(private router: Router) {}

    ngOnInit() {
        console.log('init3');
    }

    demo() {
        // this.router.navigate(['nav/home'], { replaceUrl: true });
        //this.router.navigateByUrl('/home2');
        this.router.navigate(['nav/home/home2'], { replaceUrl: true });
    }
    select() {
        console.log('sd00');
    }
}
