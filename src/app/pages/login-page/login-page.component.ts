import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

import { HttpApiService } from '../../core/services/http/httpApi.service';
import {
    IResponseLoginUser,
    IUserOnline,
} from 'src/app/Interfaces/interfacesPublics';
import { StorageService } from '../../core/services/storage/storage.service';
import { LoadingService } from '../../core/services/loadingSpinner/loading.service';
import { IProfileCompany } from '../../Interfaces/interfacesPublics';
import { AlertManagerService } from '../../core/services/alertSweet/alertManager.service';

@Component({
    selector: 'app-login-page',
    templateUrl: './login-page.component.html',
    styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
    email: string;
    password: string;
    errorMessage: string;

    // Base url
    baseurl = 'http://172.16.18.121:5000/api/values/test';
    constructor(
        private authService: AuthService,
        private router: Router,
        private httpRequest: HttpApiService,
        private storage: StorageService,
        private loading: LoadingService,
        private alert: AlertManagerService,
    ) {}

    ngOnInit() {
        this.errorMessage = '';
        // if (this.authService.isLogged()) {
        //     this.navigateTo();
        // }

        setTimeout(() => {}, 2000);
    }

    public async login(email: string, password: string) {
        this.loading.show();
        try {
            this.httpRequest.LoginUser(email, password, true).subscribe(
                data => {
                    let response: IResponseLoginUser = JSON.parse(
                        JSON.stringify(data),
                    ); // JSON.stringify(data);

                    if (response.token != '') {
                        this.storage.saveToken(response.token);
                        //console.log(response);
                        setTimeout(() => {
                            this.getProfileUserOnline();
                            this.getProfileCompany();
                            this.alert.showMsgSuccess();
                            this.loading.hide();
                            this.navigateTo();
                        }, 1000);
                    } else {
                        // throw new Error(
                        //     'When using mockLogin, login with credentials: \nemail: user\npassword:user',
                        // );
                        setTimeout(() => {
                            this.alert.showMsgFailed('Verifica los datos..');
                        }, 1000);

                        this.errorMessage = 'Datos Incorrectos..!';
                        this.loading.hide();
                    }
                },
                error => {
                    setTimeout(() => {
                        this.alert.showMsgFailed('Fallo Coneccion..');
                    }, 1000);
                    this.errorMessage = 'Fallo servidor...';
                    this.loading.hide();
                },
            );
            // const url = (await this.authService.mockLogin(
            //     email,
            //     password,
            // )) as string;
        } catch (e) {
            this.errorMessage = 'Wrong Credentials!';
            console.error('Unable to Login!\n', e);
            this.loading.hide();
        }
    }

    getProfileCompany() {
        this.httpRequest.getProfileCompany().subscribe(data => {
            let result: IProfileCompany = JSON.parse(JSON.stringify(data));
            this.storage.saveProfileCompany(result);
        });
    }
    getProfileUserOnline() {
        this.httpRequest.getProfileUserOnline().subscribe(data => {
            let result: IUserOnline = JSON.parse(JSON.stringify(data));
            this.storage.saveUserOnline(result);
        });
    }

    public navigateTo(url?: string) {
        url = url || 'nav';
        this.router.navigate([url], { replaceUrl: true });
    }
}
